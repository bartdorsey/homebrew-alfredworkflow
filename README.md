# Alfred workflow for Homebrew

This is an [Alfred](http://alfredapp.com) workflow for [Homebrew](http://brew.sh)

## How to install

Download the Homebrew.alfredworkflow file and double click on it to install into Alfred.

## How to use

This workflow supports the following commands

    b install *brewname*

This searches through all available brews and then opens a terminal and installs the brew you hit enter on

    b upgrade

This opens brew upgrade in a terminal

    b update

This opens brew update in a terminal



